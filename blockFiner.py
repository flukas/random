L = ["01-string","02-string","03-string","05-string","07-string","08-string"]
counter = None
# lastNum = None
firstString = ""
lastString = ""
L_continuous = list()
for item in L:
    currentNum = int(item[0:2])
    if counter is None:
        # startTuple
        firstString = item
        counter = currentNum
        lastString = item
        continue
    if counter + 1 == currentNum:
        # continuation of block
        lastString = item
        counter += 1
        continue
    if currentNum > counter + 1:
        # end of block
        L_continuous.append((firstString,lastString))
        firstString = item
        counter = currentNum
        lastString = item
        continue
    else:
        print ('error - not sorted or unique numbers')
# add last block
L_continuous.append((firstString,lastString))

print(L_continuous)
